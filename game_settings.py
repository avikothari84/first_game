import sys
from time import sleep
import pygame
import pygame.font
from bullet import Bullet
from ship import Ship
from obstacle import Obstacle
from treasure import Treasure
from powerup import Power
black = (0,0,0)

def check_events(player,player_num):
      for event in pygame.event.get():
            if event.type == pygame.QUIT:
                  sys.exit()
            elif event.type == pygame.KEYUP:
                  check_events_keyup(event,player,player_num)
            elif event.type == pygame.KEYDOWN:
                  check_events_keydown(event,player,player_num)

def check_events_keydown(event,player,player_num):
      if(player_num == 0):
            if event.key == pygame.K_RIGHT:
                  player.movingr = True
            elif event.key == pygame.K_LEFT:
                  player.movingl = True 
            if event.key == pygame.K_UP:
                  player.movingu = True
            elif event.key == pygame.K_DOWN:
                  player.movingd = True      
      if(player_num == 1):
            if event.key == pygame.K_d:
                  player.movingr = True
            elif event.key == pygame.K_a:
                  player.movingl = True 
            if event.key == pygame.K_w:
                  player.movingu = True
            elif event.key == pygame.K_s:
                  player.movingd = True    

def check_events_keyup(event,player,player_num):
      if(player_num == 1):
            if event.key == pygame.K_d:
                  player.movingr = False
            elif event.key == pygame.K_a:
                  player.movingl = False
            if event.key == pygame.K_w:
                  player.movingu = False
            elif event.key == pygame.K_s:
                  player.movingd = False
      if(player_num == 0):                  
            if event.key == pygame.K_RIGHT:
                  player.movingr = False
            elif event.key == pygame.K_LEFT:
                  player.movingl = False
            if event.key == pygame.K_UP:
                  player.movingu = False
            elif event.key == pygame.K_DOWN:
                  player.movingd = False
                  
def upd_screen(gm_conf, screen, player,ships,
bullets,obstacles,sb,treasures,powers):
      screen.fill(gm_conf.bg_color)
      add_partition(gm_conf,screen)
      for treasure in treasures.sprites():
            treasure.add_treasure()
      for power in powers.sprites():
            power.add_power()
      player.addplayer()
      for bullet in bullets.sprites():
            bullet.addbullet()
      for ship in ships.sprites():
            ship.add_ship()
      for obsctacle in obstacles.sprites():
            obsctacle.add_obstacle()
      sb.show_score()
      pygame.display.flip()

def upd_bullets(bullets,gm_conf):
      bullets.update()
      for bullet in bullets.copy():
            if bullet.rect.x >= gm_conf.size[0]:
                  bullets.remove(bullet)

def fire_bullet(gm_conf,screen,ships,bullets):
        if pygame.time.get_ticks()%(gm_conf.fire_rate) == 0:
                  for ship in ships:
                        new_bullet = Bullet(gm_conf,screen,ship)
                        bullets.add(new_bullet)

def create_treasure(gm_conf,screen,treasures):
      for i in gm_conf.treasure_loc:
            treasure = Treasure(gm_conf,screen)
            treasure.rect.x,treasure.rect.y = i
            treasures.add(treasure)
            
def create_power(gm_conf,screen,powers):
      for i in gm_conf.power_loc:
            power = Power(gm_conf,screen)
            power.rect.x,power.rect.y = i
            powers.add(power)

def create_bship(gm_conf,screen,ships):
      for i in range(5):
            ship = Ship(gm_conf,screen)
            ship.x = (i*i*137*i)%1200
            ship.rect.y = (70 + 160*(i))
            ships.add(ship)
      for i in range(3):
            ship = Ship(gm_conf,screen)
            ship.x = (i*i*i*137*i+700)%1200
            ship.rect.y = (70 + 160*(i))
            ships.add(ship)

def upd_ships(ships,gm_conf):     
      ships.update()

def add_partition(gm_conf, screen):
      for i in range(len(gm_conf.partition_pos)):
            pygame.draw.rect(screen,gm_conf.wood_color,gm_conf.partition_pos[i])
      
def create_obs(gm_conf,screen,obstacles):
      for i in range (len(gm_conf.obs_pos)):
            obstacle=Obstacle(gm_conf,screen)
            obstacle.rect.x = gm_conf.obs_pos[i][0]
            obstacle.rect.y = gm_conf.obs_pos[i][1]
            obstacles.add(obstacle)

def finish(screen,gm_conf,player,player_num,level):
      flag = False
      if(player_num == 0):
            if(player.rect.top == player.finishing_pt):
                  flag = True
            else:
                  flag = False
      if(player_num == 1):
            if(player.rect.bottom == player.finishing_pt):
                  flag = True
            else:
                  flag =  False   
      if(flag):
            display_text(screen,gm_conf.success_msg,gm_conf.msg_txtcolor,
            gm_conf.msg_bgcolor) 
            pygame.display.flip()
            sleep(1)
      return flag   
      
def update_conf(conf,player_num):
      conf[player_num].bullet_speed += 0.5 * conf[player_num].bullet_speed
      conf[player_num].bship_speed += 0.5 * conf[player_num].bship_speed
      conf[player_num].player_speed += 0.2 * conf[player_num].player_speed 
      conf[player_num].fire_rate -= 55

def detect_collision(gm_conf,screen,state,player,ships,
bscore,f,powers,obstacles,bullets,treasures,player_num):
      if pygame.sprite.spritecollideany(player, ships):
            state[player_num]=False
      if pygame.sprite.spritecollideany(player, obstacles):
            state[player_num]=False
      if pygame.sprite.spritecollideany(player, bullets):
            state[player_num]=False
      if pygame.sprite.spritecollideany(player, treasures):
            t = pygame.sprite.spritecollideany(player, treasures)
            treasures.remove(t)
            bscore[player_num]+=20
      if pygame.sprite.spritecollideany(player, powers):
            p = pygame.sprite.spritecollideany(player, powers)
            powers.remove(p)
            f[0]=True
      if(state[player_num] == False):
            display_text(screen,gm_conf.fail_msg,gm_conf.msg_txtcolor,
            gm_conf.msg_bgcolor)
            pygame.display.flip()
            sleep(1)
            
def update_score(bscore,score,player,player_num,sb):
      if(player_num == 0):
            if(player.rect.bottom <= 45):
                  score[player_num] = 75
            elif(player.rect.bottom <= 160):
                  score[player_num] = 65
            elif(player.rect.bottom <= 160+45):
                  score[player_num] = 60
            elif(player.rect.bottom <= 320):
                  score[player_num] = 50
            elif(player.rect.bottom <= 320+45):
                  score[player_num] = 45
            elif(player.rect.bottom <= 480):
                  score[player_num] = 35
            elif(player.rect.bottom <= 480+45):
                  score[player_num] = 30
            elif(player.rect.bottom <= 640):
                  score[player_num] = 20
            elif(player.rect.bottom <= 640+45):
                  score[player_num] = 15
            elif(player.rect.bottom <= 765):
                  score[player_num] = 5
      if(player_num == 1):
            if(player.rect.top >= 764):
                  score[player_num] = 75
            elif(player.rect.top >= 640+45):
                  score[player_num] = 65
            elif(player.rect.top >= 640):
                  score[player_num] = 60
            elif(player.rect.top >= 480+45):
                  score[player_num] = 50
            elif(player.rect.top >= 480):
                  score[player_num] = 45
            elif(player.rect.top >= 320+45):
                  score[player_num] = 35
            elif(player.rect.top >= 320):
                  score[player_num] = 30
            elif(player.rect.top >= 160+45):
                  score[player_num] = 20
            elif(player.rect.top >= 160):
                  score[player_num] = 15
            elif(player.rect.top >= 45):
                  score[player_num] = 5
      sb.sc = bscore[player_num] + score[player_num]
      sb.prep_score()

def display_text(screen,msg,text_color=(0,0,0),bg_color=None,x=400,y=400):
      screen = screen
      screen_rect = screen.get_rect()
      font = pygame.font.SysFont(None,40)
      msg = str(msg)
      img = font.render(msg,True,text_color,bg_color)
      rect = img.get_rect()
      rect.x = x
      rect.y = y
      screen.blit(img,rect) 

def make_scoreboard(screen,bscore,time):
      display_text(screen,"SCOREBOARD",black,None,450,50)
      display_text(screen,"PLAYER 1",black,None,250,250)
      display_text(screen,"PLAYER 2",black,None,750,250)
      display_text(screen,"POINTS:",black,None,12,300)
      display_text(screen,bscore[0],black,None,300,300)
      display_text(screen,bscore[1],black,None,800,300)
      display_text(screen,time[0],black,None,300,350)
      display_text(screen,time[1],black,None,800,350)
      display_text(screen,"TIME:",black,None,12,350)
      if(bscore[0]>bscore[1]):
            display_text(screen,"PLAYER 1 WINS!!",black,None,450,550)
      elif(bscore[0]<bscore[1]):
            display_text(screen,"PLAYER 2 WINS!!",black,None,450,550)
      else:
            if(time[0]<time[1]):
                  display_text(screen,"PLAYER 1 WINS!!",black,None,450,550)
            elif(time[0]>time[1]):
                  display_text(screen,"PLAYER 2 WINS!!",black,None,450,550)
            else:
                  display_text(screen,"TIE!!",black,None,450,550)

def instructions(screen):
      display_text(screen,"____INSTRUCTIONS____",(0,0,0),None,450,50)
      image1 = pygame.image.load("images/treasure.bmp")
      font = pygame.font.SysFont('comicsansms',30)
      image2 = font.render("FreeZe",True,(255,255,255),(0,10,255))
      image3 = pygame.image.load('images/bship.bmp')
      image4 = pygame.image.load("images/obstacle0.bmp")
      rect0 = pygame.Rect(0,0,12,8)
      rect0.x,rect0.y = 200,600
      plot(screen,image1,200)
      plot(screen,image2,300)
      plot(screen,image3,400)
      plot(screen,image4,500)
      pygame.draw.rect(screen,(60,60,60),rect0)
      display_text(screen,"Pick To Increase Score By 20 Points.",(0,0,0),None,300,200)
      display_text(screen," Pick To Stop The Ships and Bullets for 1 second.",(0,0,0),None,300,300)
      display_text(screen," Avoid Collision With Ship.",(0,0,0),None,300,400)
      display_text(screen," Avoid Collision With Stone.",(0,0,0),None,300,500)
      display_text(screen," Avoid Collision With Bullets.",(0,0,0),None,300,600)
      display_text(screen,"PRESS ENTER TO PLAY...! ",(0,0,0),None,370,750)
      
def plot(screen,image,y):
      rect = image.get_rect()
      rect.x,rect.y = 200,y
      screen.blit(image,rect)
      
