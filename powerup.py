import pygame
import pygame.font
from pygame.sprite import Sprite

class Power(Sprite):
      def __init__(self,gm_conf,screen):
            super().__init__()
            self.screen = screen
            self.gm_conf = gm_conf
            font = pygame.font.SysFont('comicsansms',30)
            self.image = font.render("FreeZe",True,(255,255,255),(0,10,255))
            self.rect = self.image.get_rect()
      
      def add_power(self):

            self.screen.blit(self.image,self.rect)