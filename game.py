import game_settings as gs
from pygame.sprite import Sprite,Group
from player import Player
from config import Configs
from scoreboard import Scoreboard
import sys
import pygame
player_num = 0
state=[True,True]
x = state[1] or state[0]
bscore = [0,0]
time = [0,0]
score = [0,0]
level = [0,0]
temp0,temp1=Configs(),Configs()
conf = [temp0,temp1]
def run_g0():
      p = True
      while(p):
            pygame.init()
            pygame.display.set_caption("Battlefield")
            screen=pygame.display.set_mode((1200,800))
            screen.fill((203,235,230))
            screen_rect = screen.get_rect()
            for event in pygame.event.get():
                  if event.type == pygame.QUIT:
                        sys.exit()
                  elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_RETURN:
                              run_g1()
                  elif event.type == pygame.KEYUP:
                        if event.key == pygame.K_RETURN:
                              run_g1()
            gs.instructions(screen)
            pygame.display.flip()
def run_g1():
      global x,level,player_num,state,score,bscore,conf,sb
      start_time,end_time = 0,0
      while x:
            pygame.init()
            start_time = float(pygame.time.get_ticks()/1000)
            gm_conf = conf[player_num]
            pygame.display.set_caption("Battlefield")
            screen=pygame.display.set_mode(gm_conf.size)
            bullets = Group()
            ships = Group()
            powers = Group()
            treasures = Group()
            obstacles = Group()
            power_st = 10000000000
            f = [False]
            gs.create_power(gm_conf,screen,powers)
            gs.create_bship(gm_conf,screen,ships)
            gs.create_obs(gm_conf,screen,obstacles)
            gs.create_treasure(gm_conf,screen,treasures)
            player = Player(gm_conf,screen,player_num)
            sb = Scoreboard(gm_conf,screen,score[player_num])
            while state[player_num]:
                  gs.check_events(player,player_num)
                  player.update()
                  gs.detect_collision(gm_conf,screen,state,player,ships,bscore,
                  f,powers,obstacles,bullets,treasures,player_num)
                  if(not f[0]):
                        power_st = pygame.time.get_ticks()
                        gs.fire_bullet(gm_conf,screen,ships,bullets)
                        gs.upd_bullets(bullets,gm_conf)
                        gs.upd_ships(ships,gm_conf)
                  if((pygame.time.get_ticks()-power_st) > 1000):
                        f[0] = False
                  gs.update_score(bscore,score,player,player_num,sb)
                  if(gs.finish(screen,gm_conf,player,player_num,level)):
                        end_time = float(pygame.time.get_ticks()/1000)
                        break
                  gs.upd_screen(gm_conf,screen,player,ships,bullets,obstacles
                  ,sb,treasures,powers)
                  end_time = float(pygame.time.get_ticks()/1000)
            time[player_num] += round(end_time-start_time,1)
            bscore[player_num] += score[player_num]
            score[player_num] = 0
            if(state[player_num] == True):
                  level[player_num]+=1
                  gs.update_conf(conf,player_num)
            player_num = (player_num+1)%2
            x = state[1] or state[0]
      while(True):
            pygame.init()
            pygame.display.set_caption("Battlefield")
            screen=pygame.display.set_mode((1200,800))
            screen.fill((203,235,230))
            screen_rect = screen.get_rect()
            for event in pygame.event.get():
                  if event.type == pygame.QUIT:
                        sys.exit()
            gs.make_scoreboard(screen,bscore,time)
            pygame.display.flip()
run_g0()