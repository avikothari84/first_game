# ISS Assignment 3 
### Game Development using Python and PyGame 
## _**Battlefield**_
> _AVI KOTHARI
> 2019101011_

### Read the instructions and try out my game.  


### Gameplay and Rules :  

The game has multiple rounds. 

Player 1 is at bottom and has to reach the other end by avoiding all the obstacles and in minimum time.  
Player 2 starts at the top of the screen and has to reach the bottom by avoiding all the obstacles and in minimum time.

As the player crosses a band, he/she is rewarded points according to the number of obstacles in that band, the rules are :–   
+5 for fixed obstacles.
+10 for moving obstacles. 
+20 for picking treasure.
pick FREEZE to stop the ships and bullet for 1 second.

The player with the greater score wins the game, but if the scores are equal than the player taking less time to attain that score wins. 
In the subsquent round, the speed of the moving obstacles and player increases    

### Controls: 

Keyboard `Arrow Keys` to move up, down, left and right for `PLAYER 1`. 
Keyboard `W S A D` to move up, down, left and right for  `PLAYER 2`.     
