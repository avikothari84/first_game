import pygame
from pygame.sprite import Sprite

class Obstacle(Sprite):
      def __init__(self,gm_conf,screen):
            super().__init__()
            self.screen = screen
            self.gm_conf = gm_conf
            self.image = pygame.image.load('images/obstacle0.bmp')
            self.rect = self.image.get_rect()

      def add_obstacle(self):
            self.screen.blit(self.image,self.rect)