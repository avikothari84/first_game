import pygame

class Configs():
    
      def __init__(self):
            #messages:
            self.msg_font = None
            self.msg_size = 48
            self.msg_bgcolor = (0,0,0)
            self.msg_txtcolor = (255,255,255)
            self.success_msg = "ROUND CLEARED"
            self.fail_msg = "HIT BY AN OBSTACLE"

            #screen settings:
            self.size=(1200,800)
            self.bg_color=(0,180,230)
            self.wood_color=(85,52,43)
            self.wood_size=(1200,80)

            #player settings:
            self.player_speed = 0.4

            #bullet settings:
            self.bullet_w = 12
            self.bullet_h = 8
            self.bullet_speed = 1.0
            self.bullet_color = (60,60,60)
            self.fire_rate = 500

            #bship_settings:
            self.bship_speed=0.5

            #partition settings:
            self.partition_pos = [(0,0,1200,45),(0,160,1200,45) ,
            (0,320,1200,45), (0,480,1200,45), (0,640,1200,45), (0,765,1200,45)]
            
            #treasure_pos:
            self.treasure_loc = [(520,160),(870,0),(200,320),
            (650,480),(1100,640),(300,765)]
            #power_up:
            self.power_loc = [(350,730),(600,280)]
            #obsctacles_positions:
            self.obs_pos=[[630,160],(500,480),(0,765),(100,765),
            (236,765),(500,765), (890,765),(1100,765),(1200,765)]
            for i in range(5):
                  self.obs_pos.append([(i*i*100+10*i)%1200,i*160])
                  self.obs_pos.append([(i*i*100+345*i)%1200,i*160])
                  self.obs_pos.append([(i*i*100+90*i)%1200,i*160])
                  self.obs_pos.append([1200-(i*i*100+10*i)%1200,i*160])
                  self.obs_pos.append((0,160*i))
                  self.obs_pos.append((800,160*(i%2)))
                  self.obs_pos.append([(i*i*100+901*i)%1200,0])
            self.obs_pos.append((120,160*4)) 
            self.obs_pos.append((1000,160*4))
            self.obs_pos.append((120,160*2)) 


