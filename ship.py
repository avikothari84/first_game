import pygame
from pygame.sprite import Sprite

class Ship(Sprite):

      def __init__(self,gm_conf,screen):
            super().__init__()
            self.screen = screen
            self.gm_conf = gm_conf
            self.image = pygame.image.load('images/bship.bmp')
            self.rect = self.image.get_rect()
            self.x = float(self.rect.x)
            self.y = float(self.rect.y)
            self.rect.centerx = 300

      def add_ship(self):
            self.screen.blit(self.image, self.rect)
      
      def update(self):
            self.x += self.gm_conf.bship_speed
            self.rect.x = (self.x)%1200
            