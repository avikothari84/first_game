import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):

      def __init__(self,gm_conf,screen,ship):
            super().__init__()
            self.screen=screen
            self.rect = pygame.Rect(0,0,gm_conf.bullet_w,gm_conf.bullet_h)
            self.rect.centerx = ship.rect.centerx
            self.rect.top = ship.rect.top
            self.x = float(self.rect.x)
            self.color = gm_conf.bullet_color
            self.speed = gm_conf.bullet_speed
      def update(self):
            self.x += self.speed
            self.rect.x = self.x

      def addbullet(self):
            pygame.draw.rect(self.screen,self.color,self.rect)