import pygame

class Player():
      
      def __init__(self,gm_conf,screen,player_num):
            self.screen=screen
            self.gm_conf=gm_conf
            self.image=pygame.image.load('images/player.bmp')
            self.rect = self.image.get_rect()
            self.screen_rect = screen.get_rect()
            if(player_num == 1):
                  self.finishing_pt = self.screen_rect.bottom
                  self.rect.centerx = self.screen_rect.centerx
                  self.rect.top = self.screen_rect.top
            else:
                  self.finishing_pt = self.screen_rect.top
                  self.rect.centerx = self.screen_rect.centerx
                  self.rect.bottom = self.screen_rect.bottom
            self.cenx= float(self.rect.centerx)
            self.ceny= float(self.rect.centery)
            self.movingr = False
            self.movingl = False
            self.movingu = False
            self.movingd = False

      def update(self):
            if self.movingr and self.rect.right < self.screen_rect.right:
                  self.cenx += self.gm_conf.player_speed
            elif self.movingl and self.rect.left > 0:
                  self.cenx -= self.gm_conf.player_speed
            if self.movingu and self.rect.top > 0:
                  self.ceny -= self.gm_conf.player_speed
            elif self.movingd and self.rect.bottom < self.screen_rect.bottom:
                  self.ceny += self.gm_conf.player_speed
            self.rect.centerx = self.cenx
            self.rect.centery = self.ceny

      def addplayer(self):
            self.screen.blit(self.image, self.rect)