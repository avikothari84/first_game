import pygame
from pygame.sprite import Sprite

class Treasure(Sprite):

      def __init__(self,gm_conf,screen):
            super().__init__()
            self.screen = screen
            self.gm_conf = gm_conf
            self.image = pygame.image.load("images/treasure.bmp")
            self.rect = self.image.get_rect()
            
      def add_treasure(self):
            self.screen.blit(self.image,self.rect)
      