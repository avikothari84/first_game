import pygame.font

class Scoreboard():
      
      def __init__(self,gm_conf,screen,score):
            self.screen = screen
            self.gm_conf = gm_conf
            self.sc = score
            self.screen_rect = screen.get_rect()
            self.font = pygame.font.SysFont(None, 48)
            self.prep_score()

      def prep_score(self):
            score_str = str(self.sc)
            self.score_img = self.font.render(score_str,True,
            self.gm_conf.msg_bgcolor, self.gm_conf.msg_txtcolor)
            self.score_rect = self.score_img.get_rect()
            self.score_rect.right = self.screen_rect.right -5
            self.score_rect.top = 5
      
      def show_score(self):
            self.screen.blit(self.score_img,self.score_rect)


